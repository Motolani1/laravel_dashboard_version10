
@include('layouts.app')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="login-card card-block">
                        <form class="md-float-material" method="POST" action="/forget-password">
                            @csrf
                            <div class="text-center">
                                <img src="assets/images/logo-black.png" alt="logo">
                            </div>
                            <h3 class="text-primary text-center m-b-25">Recover your password</h3>
                            @if (session('message'))
                                <div class="text-success text-center" role="alert">
                                    {{ session('message') }}
                                </div>
                            @endif
                            <br>
                            <div class="md-group">
                                <div class="md-input-wrapper">
                                    <input id="email" type="email" class="md-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <label>Email or Mobile Number</label>
                                </div>
                            </div>
                            <div class="btn-forgot">
                                <button type="submit" class="btn btn-primary btn-md waves-effect waves-light text-center">SEND RESET LINK
                                </button>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 text-center m-t-25">

                                    <a href="{{ route('login') }}" class="f-w-600 p-l-5"> Sign In Here</a>
                                </div>
                            </div>
                            <!-- end of btn-forgot class-->
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
    <!-- Warning Section Starts -->



